<?php
ini_set('memory_limit',-1);
$img=imagecreatefrompng("image.png");
if ($img) {
    $new_img = imagescale($img, 200, 100);
    if ($new_img) {
        header('Content-Type: image/png');
        imagepng($new_img);
    } else {
        exit("При сжатии изображения произошла внутрення ошибка");
    }
}
else
{
    exit("При открытии изображения произошла внутренняя ошибка");
}
?>